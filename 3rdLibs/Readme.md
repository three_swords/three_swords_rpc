# protobuf的编译

protobuf下有个third_libs文件夹，记得把abseil-cpp解压后，放入里面。

```shell
mkdir build && cd build

# 生成 makefile 文件
cmake .. -DCMAKE_CXX_STANDARD=14 -DBUILD_SHARED_LIBS=ON -Dprotobuf_BUILD_TESTS=OFF

#编译生成动态库
make -j4

# 安装
sudo make install
```


